namespace Base2art.Standard.Runtime.Remoting
{
    using Base2art.Runtime.Remoting.Specialized;
    using FluentAssertions;
    using Xunit;

    public class ProxyFeature
    {
        private class PersonConsumer
        {
            public void Consume(Person input)
            {
            }
        }

        private class EmployeeConsumer
        {
            public void Consume(Employee input)
            {
            }
        }

        private class ObjectConsumer
        {
            public void Consume(object input)
            {
            }
        }

        private class StringConsumer
        {
            public void Consume(string input)
            {
            }
        }

        private class Person
        {
        }

        private class Employee : Person
        {
        }

        public interface IConsumer<T>
        {
            void Consume(T input);
        }

        [Fact]
        public void ShouldCreate_InVariant()
        {
            {
                var obj = new PersonConsumer();
                var duck = obj.AsDuck<IConsumer<Person>>();
                duck.IsDuck.Should().BeTrue();
                duck.Instance.Should().NotBeNull();
            }
            {
                var obj = new EmployeeConsumer();
                var duck = obj.AsDuck<IConsumer<Person>>();
                duck.IsDuck.Should().BeTrue();
                duck.Instance.Should().NotBeNull();
            }
            {
                var obj = new StringConsumer();
                var duck = obj.AsDuck<IConsumer<Person>>();
                duck.IsDuck.Should().BeFalse();
                duck.Instance.Should().BeNull();
            }
            {
                var obj = new ObjectConsumer();
                var duck = obj.AsDuck<IConsumer<Person>>();
                duck.IsDuck.Should().BeFalse();
                duck.Instance.Should().BeNull();
            }
        }

        [Fact]
        public void ShouldCreate_InVariantGeneric()
        {
            {
                var obj = new StringConsumer();
                var duck = obj.AsGenericDuck<IConsumer<object>>();
                duck.IsDuck.Should().BeTrue();
                duck.SupportsType(typeof(string)).Should().BeTrue();
                duck.SupportsType(typeof(object)).Should().BeFalse();
                duck.SupportsType(typeof(Person)).Should().BeFalse();
                duck.SupportsType(typeof(Employee)).Should().BeFalse();
                Assert.NotNull(duck.Instance);
            }
            {
                var obj = new PersonConsumer();
                var duck = obj.AsGenericDuck<IConsumer<Person>>();
                duck.IsDuck.Should().BeTrue();
                duck.SupportsType(typeof(string)).Should().BeFalse();
                duck.SupportsType(typeof(object)).Should().BeFalse();
                duck.SupportsType(typeof(Person)).Should().BeTrue();
                duck.SupportsType(typeof(Employee)).Should().BeTrue();
                Assert.NotNull(duck.Instance);
            }
            {
                var obj = new EmployeeConsumer();
                var duck = obj.AsGenericDuck<IConsumer<Person>>();
                duck.IsDuck.Should().BeTrue();
                duck.SupportsType(typeof(string)).Should().BeFalse();
                duck.SupportsType(typeof(object)).Should().BeFalse();
                duck.SupportsType(typeof(Person)).Should().BeFalse();
                duck.SupportsType(typeof(Employee)).Should().BeTrue();
                Assert.NotNull(duck.Instance);
            }
            {
                var obj = new ObjectConsumer();
                var duck = obj.AsGenericDuck<IConsumer<Person>>();
                duck.IsDuck.Should().BeTrue();
                duck.SupportsType(typeof(string)).Should().BeTrue();
                duck.SupportsType(typeof(object)).Should().BeTrue();
                duck.SupportsType(typeof(Person)).Should().BeTrue();
                duck.SupportsType(typeof(Employee)).Should().BeTrue();
                Assert.NotNull(duck.Instance);
            }
        }

        [Fact]
        public void ShouldProxy()
        {
            var type = typeof(Proxies);
        }
    }
}