﻿namespace Base2art.Standard.Runtime.Remoting.Fixtures
{
    internal interface IBaby
    {
        string Name { get; }
        void Sleep();
    }
}