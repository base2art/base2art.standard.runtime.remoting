﻿namespace Base2art.Standard.Runtime.Remoting.Fixtures
{
    using System;

    internal class Baby //: IBaby
    {
        public string Name => "Annabelle";

        public void Sleep()
        {
            throw new InvalidOperationException("Teething in progress");
        }
    }

    internal class BabyIBaby : IBaby
    {
        public string Name => "Annabelle";

        public void Sleep()
        {
            throw new InvalidOperationException("Teething in progress");
        }
    }
}