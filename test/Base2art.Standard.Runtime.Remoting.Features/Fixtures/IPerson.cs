﻿namespace Base2art.Standard.Runtime.Remoting.Fixtures
{
    public interface IPerson
    {
        string Name { get; set; }
    }
}