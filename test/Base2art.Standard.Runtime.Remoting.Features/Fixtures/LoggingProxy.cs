﻿namespace Base2art.Standard.Runtime.Remoting.Fixtures
{
    using System;
    using System.Collections.Generic;
    using System.Reflection;
    using Base2art.Runtime.Remoting;
    using Base2art.Runtime.Remoting.Messaging;
    using Base2art.Runtime.Remoting.Specialized;

    public class LoggingProxy : RealProxy
    {
        private readonly object target;

        private LoggingProxy(IDuckType target, Type interfaceType)
            : base(interfaceType) => this.target = target.Instance;

        public static List<string> Debugger { get; } = new List<string>();

        public static T Wrap<T>(object target) => (T) new LoggingProxy(target.AsDuck<T>(), typeof(T)).GetTransparentProxy();

        public override IMessage Invoke(IMessage msg)
        {
            var methodCall = msg as IMethodCallMessage;

            if (methodCall != null)
            {
                return this.HandleMethodCall(methodCall); // <- see further
            }

            return null;
        }

        private IMessage HandleMethodCall(IMethodCallMessage methodCall)
        {
            Debugger.Add(string.Format("Calling `{0}`", methodCall.MethodName));

            try
            {
                var result = methodCall.MethodBase.Invoke(this.target, methodCall.InArgs);
                Debugger.Add(string.Format("Called `{0}` OK", methodCall.MethodName));
                return new ReturnMessage(result, null, 0, methodCall.LogicalCallContext, methodCall);
            }
            catch (TargetInvocationException invocationException)
            {
                var exception = invocationException.InnerException;
                Debugger.Add(string.Format("Called `{0}` {1}", methodCall.MethodName, exception.GetType().Name));
                return new ReturnMessage(exception, methodCall);
            }
        }
    }
}