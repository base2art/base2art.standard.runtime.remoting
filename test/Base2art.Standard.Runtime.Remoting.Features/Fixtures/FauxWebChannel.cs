﻿namespace Base2art.Standard.Runtime.Remoting.Fixtures
{
    using System;
    using System.Collections.Generic;
    using Base2art.Runtime.Remoting;
    using Base2art.Runtime.Remoting.Messaging;

    public class FauxWebChannel : RealProxy
    {
        private FauxWebChannel(Type interfaceType)
            : base(interfaceType)
        {
        }

        public static List<string> Debugger { get; } = new List<string>();

        public static T Create<T>() => (T) new FauxWebChannel(typeof(T)).GetTransparentProxy();

        public override IMessage Invoke(IMessage msg)
        {
            var methodCall = msg as IMethodCallMessage;

            if (methodCall != null)
            {
                return this.HandleMethodCall(methodCall); // <- see further
            }

            return null;
        }

        private IMessage HandleMethodCall(IMethodCallMessage methodCall)
        {
            if (methodCall.MethodName == nameof(RealProxyFeature.IPersonService.GetByName))
            {
                var args = methodCall.InArgs[0];
                return new ReturnMessage(
                                         new RealProxyFeature.Person {Name = args?.ToString()},
                                         new object[0],
                                         0,
                                         new LogicalCallContext(),
                                         methodCall);
            }

            return new ReturnMessage(new MissingMethodException(methodCall.MethodName), methodCall);
        }
    }
}