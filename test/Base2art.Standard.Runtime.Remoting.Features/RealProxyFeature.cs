﻿namespace Base2art.Standard.Runtime.Remoting
{
    using System;
    using Fixtures;
    using FluentAssertions;
    using Xunit;
    using Xunit.Abstractions;

    public class RealProxyFeature
    {
        public RealProxyFeature(ITestOutputHelper output) => this.output = output;

        private readonly ITestOutputHelper output;

        [Fact]
        public void ShouldLoad()
        {
            var baby = LoggingProxy.Wrap<IBaby>(new Baby());
            LoggingProxy.Debugger.Clear();

            LoggingProxy.Debugger.Should().HaveCount(0);
            baby.Name.Should().Be("Annabelle");
            LoggingProxy.Debugger.Should().HaveCount(2);
            LoggingProxy.Debugger[0].Should().Be("Calling `get_Name`");
            LoggingProxy.Debugger[1].Should().Be("Called `get_Name` OK");

            this.output.WriteLine("Name = {0}", baby.Name);
            LoggingProxy.Debugger.Should().HaveCount(4);
            LoggingProxy.Debugger[2].Should().Be("Calling `get_Name`");
            LoggingProxy.Debugger[3].Should().Be("Called `get_Name` OK");

            Action y = () => baby.Sleep();
            y.Should().Throw<InvalidOperationException>();

            LoggingProxy.Debugger.Should().HaveCount(6);
            LoggingProxy.Debugger[4].Should().Be("Calling `Sleep`");
            LoggingProxy.Debugger[5].Should().Be("Called `Sleep` InvalidOperationException");
        }

        [Fact]
        public void ShouldLoad1()
        {
            var baby = LoggingProxy.Wrap<IBaby>(new BabyIBaby());

            LoggingProxy.Debugger.Clear();

            LoggingProxy.Debugger.Should().HaveCount(0);
            baby.Name.Should().Be("Annabelle");
            LoggingProxy.Debugger.Should().HaveCount(2);
            LoggingProxy.Debugger[0].Should().Be("Calling `get_Name`");
            LoggingProxy.Debugger[1].Should().Be("Called `get_Name` OK");

            this.output.WriteLine("Name = {0}", baby.Name);
            LoggingProxy.Debugger.Should().HaveCount(4);
            LoggingProxy.Debugger[2].Should().Be("Calling `get_Name`");
            LoggingProxy.Debugger[3].Should().Be("Called `get_Name` OK");

            Action y = () => baby.Sleep();
            y.Should().Throw<InvalidOperationException>();

            LoggingProxy.Debugger.Should().HaveCount(6);
            LoggingProxy.Debugger[4].Should().Be("Calling `Sleep`");
            LoggingProxy.Debugger[5].Should().Be("Called `Sleep` InvalidOperationException");
        }

        [Fact]
        public void ShouldProxyDelegate()
        {
            var personService = FauxWebChannel.Create<IPersonService>();

            personService.GetByName("Jason").Name.Should().Be("Jason");
        }

        public interface IPersonService
        {
            Person GetByName(string jason);
        }

        public class Person : IPerson
        {
            public string Name { get; set; }
        }
    }
}