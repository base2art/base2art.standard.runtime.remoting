namespace Base2art.Standard.Runtime.Remoting
{
    using System;
    using System.Collections.Generic;
    using Base2art.Runtime.Remoting.Specialized;
    using Fixtures;
    using FluentAssertions;
    using Xunit;

    public class TypedDictionaryProxyFeature
    {
        [Fact]
        public void Should()
        {
            var readOnlyDictionary = new Dictionary<string, object>();
            readOnlyDictionary["Name"] = "SjY";
            var typed = new TypedDictionaryProxy<IPerson>(readOnlyDictionary);
            typed.Data.Name.Should().Be("SjY");
        }

        [Fact]
        public void ShouldCaseInvariant()
        {
            var readOnlyDictionary = new Dictionary<string, object>(StringComparer.OrdinalIgnoreCase);
            readOnlyDictionary["name"] = "SjY";
            var typed = new TypedDictionaryProxy<IPerson>(readOnlyDictionary);
            typed.Data.Name.Should().Be("SjY");
        }
    }
}