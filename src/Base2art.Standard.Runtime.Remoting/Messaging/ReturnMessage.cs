namespace Base2art.Runtime.Remoting.Messaging
{
    using System;
    using System.Reflection;

    public class ReturnMessage : IMethodReturnMessage
    {
        private readonly LogicalCallContext callCtx;
        private readonly IMethodCallMessage mcm;

        public ReturnMessage(
            Exception e,
            IMethodCallMessage mcm)
        {
            this.mcm = mcm;
            this.Exception = e;
        }

        public ReturnMessage(
            object returnValue,
            object[] outArgs,
            int outArgsCount,
            LogicalCallContext callCtx,
            IMethodCallMessage mcm)
        {
            this.OutArgs = outArgs;
            this.OutArgCount = outArgsCount;
            this.callCtx = callCtx;
            this.mcm = mcm;
            this.ReturnValue = returnValue;
        }

        public int OutArgCount { get; }
        public object[] OutArgs { get; }
        public Exception Exception { get; }
        public object ReturnValue { get; }

        public string Uri => this.mcm.Uri;

        public string MethodName => this.mcm.MethodName;

        public string TypeName => this.mcm.TypeName;

        public object MethodSignature => this.mcm.MethodSignature;

        public int ArgCount => this.mcm.ArgCount;

        public object[] Args => this.mcm.Args;

        public bool HasVarArgs => this.mcm.HasVarArgs;

        public LogicalCallContext LogicalCallContext => this.mcm.LogicalCallContext;

        public MethodBase MethodBase => this.mcm.MethodBase;

        public string GetArgName(int index) => this.mcm.GetArgName(index);

        public object GetArg(int argNum) => this.mcm.GetArg(argNum);

        public string GetOutArgName(int index) => this.mcm.GetArgName(index);

        public object GetOutArg(int argNum) => this.mcm.GetArg(argNum);
    }
}