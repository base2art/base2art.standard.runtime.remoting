namespace Base2art.Runtime.Remoting.Messaging
{
    using System;

    public interface IMethodReturnMessage : IMethodMessage
    {
        int OutArgCount { get; }
        object[] OutArgs { get; }
        Exception Exception { get; }
        object ReturnValue { get; }
        string GetOutArgName(int index);
        object GetOutArg(int argNum);
    }
}