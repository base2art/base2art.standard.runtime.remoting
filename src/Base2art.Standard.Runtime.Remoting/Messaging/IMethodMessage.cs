namespace Base2art.Runtime.Remoting.Messaging
{
    using System.Reflection;

    public interface IMethodMessage : IMessage
    {
        string Uri { get; }

        string MethodName { get; }

        string TypeName { get; }

        object MethodSignature { get; }

        int ArgCount { get; }

        object[] Args { get; }

        bool HasVarArgs { get; }

        LogicalCallContext LogicalCallContext { get; }

        MethodBase MethodBase { get; }

        string GetArgName(int index);

        object GetArg(int argNum);
    }
}