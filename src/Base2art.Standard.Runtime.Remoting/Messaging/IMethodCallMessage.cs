namespace Base2art.Runtime.Remoting.Messaging
{
    public interface IMethodCallMessage : IMethodMessage
    {
        int InArgCount { get; }

        object[] InArgs { get; }

        string GetInArgName(int index);

        object GetInArg(int argNum);
    }
}