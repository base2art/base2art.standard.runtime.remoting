namespace Base2art.Runtime.Remoting
{
    using System;
    using System.Reflection;
    using Base2art.Runtime.Remoting.Messaging;

    internal sealed class MethodCallMessage : IMethodCallMessage
    {
        public MethodCallMessage(MethodInfo targetMethod, object[] args, LogicalCallContext logicalCallContext)
        {
            this.MethodBase = targetMethod;
            this.Args = args;
            this.LogicalCallContext = logicalCallContext;
        }

        public object[] Args { get; }

        public LogicalCallContext LogicalCallContext { get; }

        public MethodBase MethodBase { get; }

        public string Uri => throw new NotImplementedException();

        public string MethodName => this.MethodBase.Name;

        public string TypeName => this.MethodBase.DeclaringType.Name;

        public object MethodSignature => this.MethodBase;

        public int ArgCount => this.Args.Length;

        public int InArgCount => this.Args.Length;

        public object[] InArgs => this.Args;

        public bool HasVarArgs => false;

        public string GetArgName(int index) => this.MethodBase.GetParameters()[index].Name;

        public object GetArg(int argNum) => this.Args[argNum];

        public string GetInArgName(int index) => this.MethodBase.GetParameters()[index].Name;

        public object GetInArg(int argNum) => this.Args[argNum];
    }
}