﻿namespace Base2art.Runtime.Remoting.Specialized
{
    public interface IDuckType
    {
        bool IsDuck { get; }
        object Instance { get; }
    }

    public interface IDuckType<out T> : IDuckType
    {
        new T Instance { get; }
    }
}