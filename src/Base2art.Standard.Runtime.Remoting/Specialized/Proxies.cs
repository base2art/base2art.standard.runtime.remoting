﻿namespace Base2art.Runtime.Remoting.Specialized
{
    using Internals;

    public static class Proxies
    {
        public static IDuckType<TOut> AsDuck<TOut>(this object input) => new ProxyDuckType<TOut>(input);

        public static IGenericDuckType<TOut> AsGenericDuck<TOut>(this object input) => new GenericProxyDuckType<TOut>(input);
    }
}