﻿namespace Base2art.Runtime.Remoting.Specialized
{
    using System;

    public interface IGenericDuckType<out T> : IDuckType<T>
    {
        bool SupportsType<TIn>();

        bool SupportsType(Type t);
    }
}