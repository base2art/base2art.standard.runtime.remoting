﻿namespace Base2art.Runtime.Remoting.Specialized.Internals
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using Remoting;
    using Specialized;

    internal class ProxyDuckType<T> : IDuckType<T>
    {
        private readonly object backing;

        private readonly Lazy<Dictionary<MethodInfo, MethodInfo>> matches;

        private readonly Lazy<T> proxy;

        public ProxyDuckType(object backing)
        {
            this.backing = backing;
            this.matches = new Lazy<Dictionary<MethodInfo, MethodInfo>>(this.CalculateMatches);
            this.proxy = new Lazy<T>(() => (T) this.CreateProxy(this.backing, this.matches.Value).GetTransparentProxy());
        }

        protected IReadOnlyDictionary<MethodInfo, MethodInfo> MethodMap => this.matches.Value;

        public bool IsDuck => this.matches.Value != null;
        object IDuckType.Instance => this.Instance;

        public T Instance
        {
            get
            {
                if (this.backing is T variable)
                {
                    return variable;
                }

                if (this.matches.Value == null)
                {
                    return default(T);
                }

                if (this.backing == null)
                {
                    return default(T);
                }

                return this.proxy.Value;
            }
        }

        protected virtual RealProxy CreateProxy(object backingValue, IDictionary<MethodInfo, MethodInfo> methodMap)
            => new SimpleProxy<T>(backingValue, methodMap);

        protected virtual Type GetTargetType<TIn>() => typeof(TIn);

        protected bool NamesMatch(MethodInfo x, MethodInfo y) => x.Name == y.Name;

        protected bool ReturnsMatch(MethodInfo x, MethodInfo y)
        {
            var types = this.GetGenericTypes(y, y.ReturnType, y.ReturnType);

            return types.All(z => z.GetTypeInfo().IsAssignableFrom(x.ReturnType.GetTypeInfo()));
        }

        protected bool InputsMatch(MethodInfo x, MethodInfo y)
        {
            var backingParms = x.GetParameters().Select(pi => pi.ParameterType).ToArray();
            var targetParms = y.GetParameters().Select(pi => pi.ParameterType).ToArray();
            if (backingParms.Length != targetParms.Length)
            {
                return false;
            }

            var @in = x;
            var @out = y;

            for (var i = 0; i < targetParms.Length; i++)
            {
                // compare to implementor!
                var tp = targetParms[i];
                var bp = backingParms[i];
                var returns = this.GetGenericTypes(@in, tp, bp);
                if (!returns.All(z => z.GetTypeInfo().IsAssignableFrom(bp.GetTypeInfo())))
                {
                    return false;
                }
            }

            return true;
        }

        protected Type[] GetGenericTypes(MethodInfo target, Type interfaceDeclared, Type concreteDeclared)
        {
            if (!interfaceDeclared.IsGenericParameter)
            {
                return new[] {interfaceDeclared};
            }

            var declaringTypeInfo = target.DeclaringType.GetTypeInfo();
            if (declaringTypeInfo.IsGenericType)
            {
                var genericTypeArguments = declaringTypeInfo.GenericTypeParameters;
                var args = genericTypeArguments.First(x => x.Name == interfaceDeclared.Name);

                var items = args.GetTypeInfo().GetGenericParameterConstraints();
                if (items.Length == 0)
                {
                    return new[] {interfaceDeclared == concreteDeclared ? typeof(object) : concreteDeclared};
                }

                return items;
            }

            return new[] {concreteDeclared};
        }

        private Dictionary<MethodInfo, MethodInfo> CalculateMatches()
        {
            var map = new Dictionary<MethodInfo, MethodInfo>();
            if (this.backing == null)
            {
                return map;
            }

            var backingType = this.backing.GetType();
            var targetType = this.GetTargetType<T>();
            var targetMethods = targetType.GetRuntimeMethods().ToArray();
            var backingMethods = backingType.GetRuntimeMethods().ToArray();

            foreach (var targetMethod in targetMethods)
            {
                var foundMatches = backingMethods.Where(x => this.NamesMatch(x, targetMethod)
                                                             && this.ReturnsMatch(x, targetMethod)
                                                             && this.InputsMatch(x, targetMethod))
                                                 .ToArray();
                if (!foundMatches.Any())
                {
                    return null;
                }

                map[targetMethod] = foundMatches.FirstOrDefault();
            }

            return map;
        }
    }
}