﻿namespace Base2art.Runtime.Remoting.Specialized.Internals
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using Messaging;

    internal class GenericProxy<T> : RealProxy
    {
        private readonly object backing;

        private readonly IDictionary<MethodInfo, MethodInfo> methodMatch;

        public GenericProxy(object backing, IDictionary<MethodInfo, MethodInfo> methodMatch)
            : base(typeof(T))
        {
            this.methodMatch = methodMatch;
            this.backing = backing;
        }

        public override IMessage Invoke(IMessage msg)
        {
            var methodCall = msg as IMethodCallMessage;
            if (methodCall != null)
            {
                return this.HandleMethodCall(methodCall);
                // <- see further
            }

            return null;
        }

        private IMessage HandleMethodCall(IMethodCallMessage methodCall)
        {
            try
            {
                var methodInfo = methodCall.MethodBase;
                if (methodInfo.DeclaringType == typeof(object))
                {
                    var resultActual = methodInfo.Invoke(this.backing, methodCall.InArgs);
                    return new ReturnMessage(resultActual, null, 0, methodCall.LogicalCallContext, methodCall);
                }

                var methodInfoReal = methodInfo.DeclaringType.GetGenericTypeDefinition().GetRuntimeMethods()
                                               .FirstOrDefault(x => x.Name == methodInfo.Name);
                var mapped = this.methodMatch[methodInfoReal];
                var result = mapped.Invoke(this.backing, methodCall.InArgs);
                return new ReturnMessage(result, null, 0, methodCall.LogicalCallContext, methodCall);
            }
            catch (TargetInvocationException invocationException)
            {
                var exception = invocationException.InnerException;
                return new ReturnMessage(exception, methodCall);
            }
        }
    }
}