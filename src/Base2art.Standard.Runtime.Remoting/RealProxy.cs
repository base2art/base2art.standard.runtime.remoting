namespace Base2art.Runtime.Remoting
{
    using System;
    using System.Reflection;
    using Base2art.Runtime.Remoting.Messaging;
    using Internals;

    public abstract class RealProxy
    {
        private readonly Type proxyType;

        protected RealProxy(Type proxyType)
        {
            this.proxyType = proxyType;
        }

        public abstract IMessage Invoke(IMessage msg);

        public object GetTransparentProxy()
        {
            var method = typeof(Factory).GetRuntimeMethod(nameof(Factory.CreateItemDoNotCall), new Type[0]);
            var generic = method.MakeGenericMethod(this.proxyType);
            var proxy = generic.Invoke(null, null);

            var realProxy = ((Wrapper) proxy);
            realProxy.Target = this;

            return proxy;
        }

        private class Factory
        {
            public static object CreateItemDoNotCall<T>() => DispatchProxy.Create<T, Wrapper>();
        }
    }
}