namespace Base2art.Runtime.Remoting.Internals
{
    using System;
    using System.Linq;
    using System.Reflection;
    using Messaging;
    using Remoting;

    public class Wrapper : DispatchProxy
    {
        internal RealProxy Target { get; set; }

        protected override object Invoke(MethodInfo targetMethod, object[] args)
        {
            var target = this.Target;
            if (target == null)
            {
                return null;
            }

            return Symmetrical(targetMethod, args, target);
        }

        private static object Symmetrical(MethodInfo targetMethod, object[] args, RealProxy target)
        {
            var message = new MethodCallMessage(targetMethod, args, new LogicalCallContext());
            var result = target.Invoke(message);
            if (!(result is IMethodReturnMessage rm))
            {
                return null;
            }

            if (rm.Exception != null)
            {
                throw rm.Exception;
            }

            return rm.ReturnValue;
        }
    }
}